// Note: having length != 4 will mess with layout based on how the site is styled
const bookmarks = [
  {
    title: "Daily",
    links: [
      { name: "Personal Inbox", url: "https://mail.google.com/mail/u/3/#inbox" },
      { name: "GitLab", url: "https://gitlab.com/" },
      { name: "GitHub", url: "https://github.com/" },
    ],
  },
  {
    title: "Uni",
    links: [
      { name: "Uni Inbox", url: "https://mail.google.com/mail/u/0/#inbox" },
      { name: "Uni Drive", url: "https://drive.google.com/drive/u/0/my-drive" },
      { name: "SDO", url: "https://online-edu.mirea.ru/" },
      { name: "LKS", url: "https://lk.mirea.ru/" },
      { name: "Mantis", url: "http://45.10.110.204/mantis/my_view_page.php" },
    ],
  },
  {
    title: "Prog",
    links: [
      { name: "VPS", url: "https://cp-vps.jino.ru/openvz/g5k89/info/" },
      { name: "ArchWiki", url: "https://wiki.archlinux.org/" },
      { name: "GentooWiki", url: "https://wiki.gentoo.org/" },
    ],
  },
  {
    title: "4chan",
    links: [
      { name: "/a/", url: "https://boards.4channel.org/a/" },
      { name: "/fit/", url: "https://boards.4channel.org/fit/" },
      { name: "/g/", url: "https://boards.4channel.org/g/" },
      { name: "/w/", url: "https://boards.4channel.org/w/" },
      { name: "/wsg/", url: "https://boards.4channel.org/wsg/" },
      { name: "/adv/", url: "https://boards.4channel.org/adv/" },
    ],
  }
];
